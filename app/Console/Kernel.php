<?php

namespace App\Console;

use App\Models\Pill;
use App\Models\SleepingTimetable;
use App\Events\TimeToTakePills;
use App\Events\UserWakesUp;
use App\Events\UserFallsAsleep;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [

    ];

    protected function schedule(Schedule $schedule)
    {        
        $pills = Pill::get();

        foreach ($pills as $pill) {

            if ($pill->allow_notifications) {

                $time = removeSecondsFromTime($pill->time);
                $schedule->call(function () use ($pill) {
                   event(new TimeToTakePills($pill->user, $pill));
                })->dailyAt($time);
            }
        }


        $sleepings = SleepingTimetable::get();

        foreach ($sleepings as $sleeping) {

            if ($sleeping->allow_notifications) {           

                $sleepingTimeTo = removeSecondsFromTime($sleeping->time_to);
                $schedule->call(function () use ($sleeping) {
                   event(new UserWakesUp($sleeping->user, $sleeping));
                })->dailyAt($sleepingTimeTo);

                $sleepingTimeFrom = removeSecondsFromTime($sleeping->time_from);
                $schedule->call(function () use ($sleeping) {
                   event(new UserFallsAsleep($sleeping->user, $sleeping));
                })->dailyAt($sleepingTimeFrom);

             }
        }
    }

    protected function commands()
    {
        require base_path('routes/console.php');
    }
}