<?php

function api_token() {
	return Auth::check() ? Auth::user()->api_token : null;
}

function removeSecondsFromTime($time) {
    $timeArray = explode(':', $time);
    array_pop($timeArray);
    $timeWithoutSeconds = implode(':', $timeArray);

    return $timeWithoutSeconds;
}