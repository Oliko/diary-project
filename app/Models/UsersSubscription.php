<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersSubscription extends Model
{	
	public $timestamps = false;
	
    protected $fillable = [
        'user_id',
        'subscription_id',
    ];

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

	public function subscriptions()
    {
	    return $this->hasMany('App\Models\Subscription');
    }
}