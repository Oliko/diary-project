<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SleepingTimetable extends Model
{
	public $timestamps = false;
	
    protected $fillable = [
        'time_from', 
        'time_to',
        'allow_notifications',
    ];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
