<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pill extends Model
{
	public $timestamps = false;

    protected $fillable = [
        'name', 
        'dose',
        'notes',
        'time',
        'allow_notifications',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
