<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	public $timestamps = false;
	
    protected $fillable = [
        'name', 
        'carbs',
        'glycemic_index',
    ];

    public function bread_unity() 
    {
        return $this->carbs / 10;
    }
}
