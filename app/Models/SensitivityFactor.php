<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SensitivityFactor extends Model
{
	public $timestamps = false;
	
    protected $fillable = [
        'time_from', 
        'time_to',
        'value',
    ];
}
