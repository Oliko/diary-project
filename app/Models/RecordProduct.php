<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecordProduct extends Model
{
	public $timestamps = false;
	
    protected $fillable = [
        'name', 
        'carbs'
    ];
}
