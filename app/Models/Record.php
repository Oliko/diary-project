<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
	public $timestamps = false;
	
    protected $fillable = [
        'time',
        'date',
        'sugar_level', 
        'bread_unity',
        'insulin',
        'pills',
    ];

    public function products()
    {
        return $this->hasMany('App\Models\RecordProduct');
    }
}
