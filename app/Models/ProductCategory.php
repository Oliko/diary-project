<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
	public $timestamps = false;
	
    protected $fillable = [
        'name', 
    ];

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }
}
