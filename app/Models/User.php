<?php

namespace App\Models;

use DB;
use Auth;
use App\Models\Pill;
use App\Models\Record;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 
        'email', 
        'api_token',
        'password',
        'admin',
    ];

    protected $hidden = [
        'password', 
        'remember_token',
    ];

    public function isAdmin()
    {
        return $this->admin;
    }

    public function isSubscribed($subscription)
    {
        return $this->subscriptions()->where('subscriptions.id', $subscription->id)->count() > 0;
    }

    public function coefficients()
    {
        return $this->hasMany('App\Models\Coefficient');
    }

    public function sensitivities()
    {
        return $this->hasMany('App\Models\SensitivityFactor');
    }

    public function pills()
    {
        return $this->hasMany('App\Models\Pill');
    }

    public function sleeping()
    {
        return $this->hasOne('App\Models\SleepingTimetable');
    }

    public function records()
    {
        return $this->hasMany('App\Models\Record');
    }

    public function subscriptions()
    {
        return $this->belongsToMany('App\Models\Subscription', 'users_subscriptions');
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setSleeping(array $data)
    {
        $sleepingTimetable = $this->sleeping;
        $sleepingTimetable->time_from = $data['time_from'];
        $sleepingTimetable->time_to = $data['time_to'];
        $sleepingTimetable->allow_notifications = $data['allow_notifications'];
        $sleepingTimetable->save();
    }

    public function setPills(array $data)
    {
        $pillsModels = [];

        foreach ($data as $pill) {
            $pillsModels[] = new Pill([
                'name' => $pill['name'], 
                'dose' => $pill['dose'],
                'notes' => $pill['notes'],
                'time' => $pill['time'],
                'allow_notifications' => $pill['allow_notifications']
            ]);
        }

        DB::transaction(function () use ($pillsModels) {
            $this->pills()->delete();
            $this->pills()->saveMany($pillsModels);
        });
    }

    public function setCoefficients(array $data)
    {
        $coefficientsModels = [];

        foreach ($data as $coefficient) {
            $coefficientsModels[] = new Coefficient([
                'time_from' => $coefficient['time_from'],
                'time_to' => $coefficient['time_to'],
                'value' => $coefficient['value']
            ]);
        }

        DB::transaction(function () use ($coefficientsModels) {
            $this->coefficients()->delete();
            $this->coefficients()->saveMany($coefficientsModels);
        });
    }

    public function setSensitivities(array $data)
    {
        $sensitivitiesModels = [];

        foreach ($data as $sensitivity) {
            $sensitivitiesModels[] = new SensitivityFactor([
                'time_from' => $sensitivity['time_from'],
                'time_to' => $sensitivity['time_to'],
                'value' => $sensitivity['value']
            ]);
        }

        DB::transaction(function () use ($sensitivitiesModels) {
            $this->sensitivities()->delete();
            $this->sensitivities()->saveMany($sensitivitiesModels);
        });
    }

    // public function setSubscriptions(array $data)
    // {
    //     $subscriptionsModels = [];

    //     foreach ($data as $subscription) {
    //         $subscriptionsModels[] = new UsersSubscription([
    //             'user_id' => Auth::user()->id,
    //             'subscription_id' => $subscription['id']
    //         ]);
    //     }

    //     DB::transaction(function () use ($subscriptionsModels) {
    //         $this->subscriptions()->delete();
    //         $this->subscriptions()->saveMany($subscriptionsModels);
    //     });
    // }

    public function addRecord(array $recordData, string $date)
    {
        DB::transaction(function () use ($recordData, $date) {
            $record = Record::firstOrNew(['id' => $recordData['id']]);
            $record->date = $date;
            $record->time = $recordData['time'];
            $record->sugar_level = $recordData['sugar_level'];
            $record->bread_unity = $recordData['bread_unity'];
            $record->insulin = $recordData['insulin'];
            $record->pills = $recordData['pills'];
            $this->records()->save($record);

            $products = $recordData['products'];
            $record->products()->delete();

            foreach ($products as $product) {
                $recordProduct = new RecordProduct;
                $recordProduct->name = $product['name'];
                $recordProduct->carbs = $product['carbs'];
                $record->products()->save($recordProduct);
            }
        });
    }
}