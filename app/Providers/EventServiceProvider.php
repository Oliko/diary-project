<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        'App\Events\TimeToTakePills' => [
            'App\Listeners\SendMailNotification',
        ],

        'App\Events\UserWakesUp' => [
            'App\Listeners\SendAwakenessNotification',
        ],

        'App\Events\UserFallsAsleep' => [
            'App\Listeners\SendFallingAsleepNotification',
        ],
    ];

    public function boot()
    {
        parent::boot();
    }
}
