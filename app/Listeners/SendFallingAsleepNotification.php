<?php

namespace App\Listeners;

use Mail;
use App\Mail\FallingNotifications;
use App\Events\UserFallsAsleep;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendFallingAsleepNotification
{
    public function __construct()
    {
        
    }

    public function handle(UserFallsAsleep $event)
    {
        Mail::to($event->user->email)
            ->send(new FallingNotifications($event->user->name, $event->time));  
    }
}
