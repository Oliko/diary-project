<?php

namespace App\Listeners;

use Mail;
use App\Mail\AwakenessNotifications;
use App\Events\UserWakesUp;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAwakenessNotification
{
    public function __construct()
    {
        
    }

    public function handle(UserWakesUp $event)
    {
        Mail::to($event->user->email)
            ->send(new AwakenessNotifications($event->user->name, $event->time));  
    }
}
