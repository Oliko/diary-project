<?php

namespace App\Listeners;

use Mail;
use App\Mail\PillsNotifications;
use App\Events\TimeToTakePills;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailNotification
{
    public function __construct()
    {
        
    }

    public function handle(TimeToTakePills $event)
    {        
        Mail::to($event->user->email)
            ->send(new PillsNotifications($event->user->name, $event->pill));  
    }
}
