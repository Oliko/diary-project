<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PillsNotifications extends Mailable
{
    use Queueable, SerializesModels;

    protected $username;
    protected $pill;

    public function __construct($username, $pill)
    {
        $this->username = $username;
        $this->pill = $pill;
    }

    public function build()
    {
        return $this->from('diabetic.notifications@gmail.com')
                    ->view('emails.notifications')
                    ->with('username', $this->username)
                    ->with('pill', $this->pill);
    }
}
