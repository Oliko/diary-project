<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FallingNotifications extends Mailable
{
    use Queueable, SerializesModels;

    protected $username;
    protected $time;

    public function __construct($username, $time)
    {
        $this->username = $username;
        $this->time = $time;
    }

    public function build()
    {
        return $this->from('diabetic.notifications@gmail.com')
                    ->view('emails.fallingNotifications')
                    ->with('username', $this->username)
                    ->with('time', $this->time);
    }
}
