<?php

namespace App\Http\Controllers;

use App\Models\Pill;
use App\Events\TimeToTakePills;
use App\Mail\PillsNotifications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class PagesController extends Controller
{
	public function home()
    { 
    	return view('pages.home');
    }

}
