<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
	public function products()
    {
    	$categories = ProductCategory::get();

        $user = Auth::user();

    	return view('pages.products')
    		->with('categories', $categories)
    		->with('user', $user);
    }
}
