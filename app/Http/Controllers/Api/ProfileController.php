<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Models\Coefficient;
use App\Models\SensitivityFactor;
use App\Models\Pill;
use App\Models\Record;
use App\Models\Subscription;
use Illuminate\Http\Request;

class ProfileController extends ApiController
{
    public function user()
    {
        $user = Auth::user();

        return $this->respond($user);
    }

    public function coefficients()
    {
        $coefficients = Auth::user()->coefficients()->get();

        return $this->respond($coefficients);
    }

    public function sensitivities()
    {
        $sensitivities = Auth::user()->sensitivities()->get();

        return $this->respond($sensitivities);
    }

    public function pills()
    {
        $pills = Auth::user()->pills()->get();

        return $this->respond($pills);
    }

    public function sleeping()
    {
        $sleeping = Auth::user()->sleeping()->first();

        return $this->respond($sleeping);
    }

    public function records()
    {
        $records = Auth::user()->records()->get();

        return $this->respond($records);
    }

    public function saveData(Request $request)
    {
        $user = Auth::user();

        $name = $request->user['name'];
        $user->name = $name;

        $sleeping = $request->sleeping;
        $user->setSleeping($sleeping);

        $pills = $request->pills;
        $user->setPills($pills);

        $coefficients = $request->coefficients;
        $user->setCoefficients($coefficients);

        $sensitivities = $request->sensitivities;
        $user->setSensitivities($sensitivities);

        // $subscriptions = $request->subscriptions;
        // $user->setSubscriptions($subscriptions);


        $user->save();
    }

    public function subscriptions()
    {
        $subscriptions = Subscription::get();

        $userSubscriptions = $subscriptions->map(function($subscription){
            return [
                'id' => $subscription->id,
                'name' => $subscription->name,
                'subscribed' => Auth::user()->isSubscribed($subscription)
            ];
        });

        return $this->respond($userSubscriptions);
    }


}
