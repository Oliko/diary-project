<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    private $statusCode = 200;

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function respondNotFound($message = "Not Found")
    {
        return $this->setStatusCode(404)->responseWithError($message);
    }

    public function respond($data, $success = true)
    {
        return Response::json(['success' => $success, 'response' => $data], $this->getStatusCode());
    }

    public function respondWithError($message)
    {
        return $this->respond(
            ['status_code' => $this->getStatusCode(), 'message' => $message], 
            $success = false
        );
    }
}
