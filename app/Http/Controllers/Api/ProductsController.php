<?php

namespace App\Http\Controllers\Api;

use Auth;
use DB;
use App\Models\{Product, ProductCategory};
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends ApiController
{
    public function getProducts()
    {
        $products = DB::table('products')->get();
        
        return $this->respond($products);
    }

    public function add(Request $request)
    {
    	$product = new Product;
    	$product->name = $request->name;
    	$product->carbs = $request->carbohydrates_amount;
    	$product->glycemic_index = $request->glycemic_index;

    	ProductCategory::find($request->category)->products()->save($product);

        return $this->respond($product);
    }

}