<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class AllUsersController extends ApiController
{
    public function users()
    {
        $users = DB::table('users')->select('name', 'email', 'created_at')->get();
    }

}
