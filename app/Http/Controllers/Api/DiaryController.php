<?php

namespace App\Http\Controllers\Api;

use Auth;
use DB;
use App\Models\Record;
use App\Models\RecordProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DiaryController extends ApiController
{
    public function records(Request $request)
    {
        $date = $request->date ?? date('Y-m-d');

        $records = Auth::user()->records()->with('products')->where('date', $date)->get();
        
        return $this->respond($records);
    }
    
    public function addRecord(Request $request)
    {
        $user = Auth::user();

        $record = $request->record;
        $date = $request->date;

        $user->addRecord($record, $date);

        return $this->respond(true);
    }

    public function deleteRecord(Request $request) 
    {
        $record = $request->record;
        Record::where('id', $record['id'])->delete();

        return $this->respond(true);
    }
}
