<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\User;
use App\Models\Subscription;
use App\Models\UsersSubscription;

class AllUsersController extends Controller
{
	public function users()
    { 
        $users = User::select('name', 'email', 'created_at')->get();
        $allSubscriptions = Subscription::get();
        $users_subscriptions = UsersSubscription::get();

        $subscription = User::find(1)->subscriptions()->orderBy('name')->get(); 

        $subscriptions = User::join('users_subscriptions', 'users.id', '=', 'users_subscriptions.user_id')->get();

        // dd($subscriptions);
        // подписка авториз юзера
        // dd($users_subscriptions);

    	return view('pages.users')
			->with('users', $users)
            ->with('users_subscriptions', $users_subscriptions)
            ->with('subscriptions', $subscriptions);
    }
}
