<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Subscription;
use App\Models\UsersSubscription;
use DB;

class ProfileController extends Controller
{
    public function profile()
    {   	
    	return view('users.profile');
    }
}