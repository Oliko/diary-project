<?php

namespace App\Events;

use App\Models\SleepingTimetable;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserFallsAsleep
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $time;

    public function __construct($user, $time)
    {
        $this->user = $user;       
        $this->time = $time;        
    }

    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
