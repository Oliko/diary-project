<?php

namespace App\Events;

use App\Models\Pill;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TimeToTakePills
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $pill;

    public function __construct($user, $pill)
    {
        $this->user = $user;
        $this->pill = $pill;        
    }

    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
