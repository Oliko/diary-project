<div class="notification">
	Привіт, {{ $username }}!<br>
	Зараз {{ $pill->time }} година. Нагадуємо, що треба випити такі ліки : {{ $pill->name}}.<br>
	Доза : {{ $pill->dose}}.<br>
	Ваші нотатки : {{ $pill->notes}}.<br>
</div>