@extends('layouts.main')
@section('content')
<div id="users">
	<div class="container">
		<div class="heading" style="padding-top: 50px;">		
			Зареєстровані користувачі
		</div>
		<hr>
			<table class="table table-bordered table-hover products">
				<thead>
					<tr>
						<th>Імя користувача</th>
						<th>Пошта користувача</th>
						<th>Дата реєстрації користувача</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($users as $user)
					<tr>
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->created_at }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>




	</div>
</div>
@stop