@extends('layouts.main')

@section('content')
<div id="products">
	<div class="container">
		<div class="heading">		
			Енергетична цінність у продуктах харчування		
		</div>
		<hr>

		@foreach ($categories as $category)
			<div class="heading">		
				{{ $category->name }}	
			</div>
			<table class="table table-bordered table-hover product in products">
				<thead>
					<tr>
						<th class="name">Назва продукту</th>
						<th>Кількість вуглеводів</th>
						<th>Глікемічний індекс</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($category->products as $product)
					<tr>
						<td>{{ $product->name }}</td>
						<td>{{ $product->carbs }}</td>
						<td>{{ $product->glycemic_index }}</td>
    					@if (Auth::check() && Auth::user()->isAdmin())
    						<td>    
    						
						<a class="remove" @click="removeField(products, product)"><i class="fa fa-times" aria-hidden="true"></i></a>


								<!-- <form action="/api/delete/product" type="delete" id="deleteProduct">
							       <button type="submit" class="btn btn-danger">Видалити</button>
							    </form> -->
    						</td>
   						@endif
					</tr>
					@endforeach
				</tbody>
			</table>

		@endforeach
	</div>

    @if (Auth::check() && Auth::user()->isAdmin())
    	<div class="newSection">
	   		<div class="container">
	   		 	<div class="heading">		
					Новий продукт		
				</div>
				<hr>
	   		 	<div class="newProduct"> 	
					<form action="/api/add/product" type="POST" id="addProduct">
						<div class="form-group">
							<label>Оберіть категорію</label>
							<select name="category" class="form-control">
								@foreach ($categories as $category)
								  <option value="{{ $category->id }}">{{ $category->name }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Назва продукту</label>
							<input type="text" class="form-control" name="name">	
						</div>
						<div class="form-group">
							<label>Кількість вуглеводів</label>
							<input type="text" class="form-control" name="carbohydrates_amount">	
						</div>
						<div class="form-group">
							<label>Глікемічний індекс</label>
							<input type="text" class="form-control" name="glycemic_index">				
						</div>
						<button class="btn saveBtn">Додати</button>
					</form>
				</div>
			</div>
	    </div>
    @endif

</div>
@stop

@push('scripts')
	<script src="{{ asset('js/pages/products.js') }}"></script>
@endpush