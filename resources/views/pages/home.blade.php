@extends('layouts.main')

@section('content')

<div id="home">

	<header class="mask">			
		Ласкаво просимо на наш сайт!	
	</header>

	<div class="info">
		<div class="container">
			<div class="heading">	
				Наші можливості
			</div>		
			<div class="sections row clearfix">

				<div class="col-md-3 col-sm-6">
					<div class="section">
						<img src="/images/register.png" alt="">
						<div class="text">
							Зареєструтесь на користуйтесь повним функціоналом!
						</div>
					</div>					
				</div>

				<div class="col-md-3 col-sm-6">
					<div class="section">
						<img src="/images/profile.png" alt="">
						<div class="text">
							Заповнюйте інформацію про себе в особистому профілі!
						</div>
					</div>
				</div>

				<div class="col-md-3 col-sm-6">
					<div class="section">
						<img src="/images/products.png" alt="">
						<div class="text">
							Рахуйте кількість вуглеводів продуктів харчування!
						</div>
					</div>
				</div>

				<div class="col-md-3 col-sm-6">
					<div class="section">
						<img src="/images/diary.png" alt="">
						<div class="text">
							Робіть записи щодня у власному щоденнику!
						</div>
					</div>
				</div>

			</div>
		</div>		
	</div>

    @if (Auth::guest())
	<div class="container">
		<div class="heading">		
			Зареєструйтесь прямо зараз!
		</div>
		<div class="register">
        	<div class="col-md-8 col-md-offset-2">
            	<div class="panel panel-default" style="margin-bottom: 50px;">
                    <div class="panel-body">			
			            <form role="form" method="POST" action="{{ route('register') }}">
			                {{ csrf_field() }}

			                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
			                    <label>Ім'я</label>
			                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" required >
			                    @if ($errors->has('name'))
			                    <div class="alert alert-danger">{{ $errors->first('name') }}</div>
			                    @endif
			                </div>

			                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
			                    <label>Пошта</label>
			                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
			                    @if ($errors->has('email'))
			                    <div class="alert alert-danger">{{ $errors->first('email') }}</div>
			                    @endif
			                </div>

			                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
			                    <label>Пароль</label>
			                    <input type="password" class="form-control" name="password" required>
			                    @if ($errors->has('password'))
			                    <div class="alert alert-danger">{{ $errors->first('password') }}</div>
			                    @endif
			                </div>

			                 <button type="submit" class="btn btn-primary">Зареєструватись</button>
			            </form>
           			</div>
           		</div>
            </div>

		</div>
	</div>
    @endif
</div>
@stop