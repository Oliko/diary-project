<!DOCTYPE html> 
<html lang="{{ config('app.locale') }}"> 
<head> 
	<meta charset="utf-8"> 
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
	<meta name="csrf-token" content="{{ csrf_token() }}"> 

	<title>@yield('title') - {{ config('app.name', 'Laravel') }}</title> 
	<link href="{{ asset('css/app.css') }}" rel="stylesheet"> 
	<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"> 

	<script> 
		window.Laravel = {!! json_encode([ 
			'csrfToken' => csrf_token(), 
			'apiToken' => api_token(), 
		]) !!}; 
	</script> 
</head> 

<body> 
	<header> 
		<div class="container"> 
			@include('partials.nav')
		</div> 
	</header> 


	@yield('content') 

	<footer>
		<div class="container">
			Copyright © Diabetic diary 2017
		</div>
	</footer>

	<script src="{{ asset('js/app.js') }}"></script> 

	@stack('scripts')
</body> 
</html>