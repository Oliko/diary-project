<div class="profile-general">
	<div class="container">
		
		<div class="row clearfix">
			<div class="col-sm-6">
				<div class="heading">Основні відомості</div>
			</div>
			<div class="col-sm-6 text-right">
				<button type="button" class="btn saveBtn" @click="saveAllData">Зберегти</button>
			</div>
		</div>
		
		<hr>
		<div class="row clearfix">
			<div class="col-sm-6">
				<div class="form-group">
					<label>Ваше ім'я</label>
					<input type="text" class="form-control" v-model="user.name">
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label>Ваша пошта</label>
					<input type="text" class="form-control" v-model="user.email">
				</div>
			</div>
		</div>
	</div>
	
</div>


<div class="password">
	<div class="container">
		
		<div class="row clearfix">
			<div class="col-sm-6">
				<div class="heading">Пароль</div>
			</div>
			<div class="col-sm-6 text-right">
				<button type="button" class="btn saveBtn" @click="saveAllData">Зберегти</button>
			</div>
		</div>
		
		<hr>
		
		<div class="row clearfix">
			<div class="col-sm-6">
				<div class="form-group">
					<label>Поточний пароль</label>
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label>Новий пароль</label>
					<input type="text" class="form-control">
				</div>
			</div>
		</div>
		
	</div>
</div>


<div class="container">
	<div class="subscriptions">	
		<div class="row clearfix">
			<div class="col-sm-6">
				<div class="heading">Новини, на які ви підписані</div>
			</div>
			<div class="col-sm-6 text-right">
				<button type="button" class="btn saveBtn" @click="saveAllData">Зберегти</button>
			</div>
		</div>

		<hr>
		
		<div class="form-group">	
			<label class="custom-control custom-checkbox form-group" v-for="subscription in subscriptions">
		        @{{ subscription.name }}
				<input type="checkbox" :name="subscription.id" class="custom-control-input" v-model="subscription.subscribed">
				<span class="custom-control-indicator"></span>
			</label>		
		</div>
	</div>
</div>