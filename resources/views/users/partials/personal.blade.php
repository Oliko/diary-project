<div class="insulin-data">
	<div class="container">
		
		<div class="personal-section">

			<div class="section-head">
				<div class="row clearfix">
					<div class="col-sm-6">
						<div class="heading">Коефіцієнти</div>
					</div>
					<div class="col-sm-6 text-right">
						<button type="button" class="btn addBtn" @click="addField(coefficients)">Додати новий</button>
						<button type="button" class="btn saveBtn" @click="saveAllData">Зберегти</button>
					</div>
				</div>
			</div>			
			
			<div class="row clearfix">
				<div class="col-sm-4" v-for="coefficient in coefficients.list">
					<div class="profile-coefficient">
						<a class="remove" @click="removeField(coefficients, coefficient)"><i class="fa fa-times" aria-hidden="true"></i></a>
						<div class="param clearfix row">
							<div class="col-sm-8">
								<label>З котрої години</label>
							</div>
							<div class="col-sm-4 text-right">
								<input type="time" v-model="coefficient.time_from">
							</div>
						</div>
						<div class="param clearfix row">
							<div class="col-sm-8">
								<label>До котрої години</label>
							</div>
							<div class="col-sm-4 text-right">
								<input type="time" v-model="coefficient.time_to">
							</div>
						</div>
						<div class="param clearfix row">
							<div class="col-sm-8">
								<label>Коефіцієнт</label>
							</div>
							<div class="col-sm-4 text-right">
								<input type="text" v-model="coefficient.value">
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="section-head">
				<div class="row clearfix">
					<div class="col-sm-6">
						<div class="heading">Фактори чутливості</div>
					</div>
					<div class="col-sm-6 text-right">
						<button type="button" class="btn addBtn" @click="addField(sensitivities)">Додати новий</button>
						<button type="button" class="btn saveBtn" @click="saveAllData">Зберегти</button>
					</div>
				</div>
			</div>		


			<div class="row clearfix" >
				<div class="col-sm-4" v-for="sensitivity in sensitivities.list">
					<div class="profile-sensitivity">
						<a class="remove" @click="removeField(sensitivities, sensitivity)"><i class="fa fa-times" aria-hidden="true"></i></a>
						<div class="param clearfix row">
							<div class="col-sm-8">
								<label>З котрої години</label>
							</div>
							<div class="col-sm-4 text-right">
								<input type="time" v-model="sensitivity.time_from">
							</div>
						</div>
						<div class="param clearfix row">
							<div class="col-sm-8">
								<label>До котрої години</label>
							</div>
							<div class="col-sm-4 text-right">
								<input type="time" v-model="sensitivity.time_to">
							</div>
						</div>
						<div class="param clearfix row">
							<div class="col-sm-8">
								<label>Коефіцієнт</label>
							</div>
							<div class="col-sm-4 text-right">
								<input type="text" v-model="sensitivity.value">
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>		
		
	</div>
</div>



<div class="pills">
	<div class="container">
		<div class="heading">
			<div class="row clearfix">
				<div class="col-sm-6">
					Ліки
				</div>
				<div class="col-sm-6 text-right">
					<button type="button" class="btn addBtn" @click="addField(pills)">Додати нові</button>
					<button type="button" class="btn saveBtn" @click="saveAllData">Зберегти</button>
				</div>
			</div>
		</div>
		<hr>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Назва</th>
					<th>Доза</th>
					<th>Нотатки</th>
					<th>Час прийому</th>
					<th>Нагадування</th>
					<th></th>
				</tr>
			</thead>
			<tr v-for="pill in pills.list">
				<td><input type="text" class="form-control" v-model="pill.name"></td>
				<td><input type="text" class="form-control" v-model="pill.dose"></td>
				<td><input type="text" class="form-control" v-model="pill.notes"></td>
				<td><input type="time" class="form-control" v-model="pill.time"></td>
				<td>
					<label class="custom-control custom-checkbox form-group">
						<input type="checkbox" :name="pill.id" class="custom-control-input" v-model="pill.allow_notifications">
						<span class="custom-control-indicator"></span>
					</label>
				</td>
				<td>
					<i class="fa fa-window-close deleteIcon" aria-hidden="true" @click="removeField(pills, pill)"></i>
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="sleeping">
	<div class="container">
		<div class="heading">
			<div class="row clearfix">
				<div class="col-sm-6">
					Сон
				</div>
				<div class="col-sm-6 text-right">
					<button type="button" class="btn saveBtn" @click="saveAllData">Зберегти</button>
				</div>
			</div>
		</div>
		<hr>
		<div class="sleepingTime col-sm-6">
			З котрої години
			<input type="time" class="form-control" v-model="sleeping.time_from">
			До котрої години
			<input type="time" class="form-control" v-model="sleeping.time_to">
		</div>
		<div class="sleepingNotifications col-sm-6">
			<div class="form-group">
				<label class="custom-control custom-checkbox">
					<input type="checkbox" class="custom-control-input" v-model="sleeping.allow_notifications">
					<span class="custom-control-indicator"></span>
					Отримувати нагадування про ліки
				</label>
			</div>				
		</div>
	</table>
</div>
</div>