@extends('layouts.main')

@section('content')
<div id="profile">

	<div class="sub-header">
		<div class="container">
			<div class="user-name">
				@{{ user.name }}
			</div>
		</div>
	</div>

	<div class="profile-tabs">
		<div class="container">
			<ul class="list-unstyled clearfix">
				<li class="active"><a data-toggle="tab" href="#general">Основне</a></li>
				<li><a data-toggle="tab" href="#personal">Особисті данні</a></li>
			</ul>
		</div>
	</div>
	
	<div class="tab-content">

		<div id="general" class="tab-pane fade in active">
			@include('users.partials.general')
		</div>

		<div id="personal" class="tab-pane fade">		
			@include('users.partials.personal')
		</div>
	</div>
</div>


</div>
@stop

@push('scripts')
<script src="{{ asset('js/pages/profile.js') }}"></script> 
@endpush