@extends('layouts.main')

@section('content')

<div id="diary">
	<div class="container">
		<div class="heading">
			<div class="row clearfix">
				<div class="col-sm-6">
					Зробіть нові записи!
				</div>
				<div class="col-sm-6 text-right">
					<form action="/records" method="GET" id="chooseDiaryDay">
						<input type="date" name="date" @change="reloadRecords" v-model="date" >
					</form>
				</div>
			</div>
		</div>
		<hr>
		<div class="table-responsive diary-table">
			<table class="table table-hover table-bordered records">
				<thead>
					<tr>
						<th class="text-center" style="width: 10%;">Час</th>
						<th class="text-center" style="width: 12%;">Рівень цукру</th>
						<th class="text-center" style="width: 27%;">Продукти</th>
						<th class="text-center" style="width: 10%;">Вуглеводи</th>
						<th class="text-center" style="width: 8%;">Інсулін</th>
						<th class="text-center" style="width: 26%;">Ліки</th>
						<th class="" style="width: 5%;"></th>
					</tr>
				</thead>
				<tbody>
				<tr is="record" v-for="record in records.list" :record="record"></tr>
				<tr is="record" :record="records.new"></tr>
		</tbody>
	</table>
</div>
</div>
</div>

<script type="text/x-template" id="record-template">
	<tr>
		<td> <input type="time" class="full" v-model="record.time" @change="saveRecord"> </td>
		<td> <input type="text" class="full" placeholder="m/mol" v-model="record.sugar_level" @change="saveRecord"> </td>
		<td class="products">
			<ol @click="addProduct">
				<li v-for="product in record.products" @click.stop>
					<input type="text" class="" v-model="product.name" @change="saveRecord">
				</li>
			</ol>
		</td>
		<td> <input type="text" class="full" placeholder="х/одиниці" v-model="record.bread_unity" @change="saveRecord"> </td>
		<td> <input type="text" class="full" placeholder="одиниць" v-model="record.insulin" @change="saveRecord"> </td>
		<td> <input type="text" class="full" placeholder="" v-model="record.pills" @change="saveRecord"> </td>
		<td class="remove"> <i class="fa fa-remove" aria-hidden="true" @click="removeRecord"></i></td>
	</tr>
</script>

@stop

@push('scripts')
<script src="{{ asset('js/pages/diary.js') }}"></script>
@endpush