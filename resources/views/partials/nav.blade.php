<nav class="navbar navbar-custom">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ route('home') }}">Diabetic diary</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ route('home') }}">Головна</a></li>
            <li><a href="{{ route('products') }}">Продукти</a></li>
            <li><a href="{{ route('profile') }}">Профіль</a></li>
            <li><a href="{{ route('diary') }}">Щоденник</a></li>
            
            @if (Auth::guest())
            <li><a href="{{ route('login') }}">Увійти</a></li>
            <li><a href="{{ route('register') }}">Зареєструватись</a></li>
            @elseif (Auth::check() && Auth::user()->isAdmin())
            <li><a href="{{ route('users') }}">Користувачі</a></li>
            <li><a href="/logout">Вийти</a></li>
            @else
            <li><a href="/logout">Вийти</a></li>
            @endif
        </ul>
    </div>
</nav>