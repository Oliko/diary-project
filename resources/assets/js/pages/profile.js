const profile = new Vue({
    el: '#profile',

    mounted() {
        this.fetchData();
    },

    data: {
        success: false,

        user: {},

        coefficients: {
            template: {
                time_from: null,
                time_to: null,
                value: null,
            },
            list: [],
        },

        sensitivities: {
            template: {
                time_from: null,
                time_to: null,
                value: null,
            },
            list: [],
        },

        pills: {
            template: {
                name: null,
                dose: null,
                notes: null,
                time: null,
                allow_notifications: true,
            },
            list: [],
        },

        sleeping: {
            time_from: null,
            time_to: null,
            allow_notifications: true,
        },

        subscriptions: [],

    },
    methods: {
        fetchData() {
            let options = {
                params: {
                    api_token: Laravel.apiToken
                }
            };

            this.$http.get('/api/profile', options).then((response) => {
                let user = response.data.response;
                this.user = user;
            });

            this.$http.get('/api/profile/sleeping', options).then((response) => {
                let sleeping = response.data.response;
                this.sleeping = sleeping;
            });

            this.$http.get('/api/profile/pills', options).then((response) => {
                let pills = response.data.response;
                this.pills.list = pills;
            });

            this.$http.get('/api/profile/coefficients', options).then((response) => {
                let coefficients = response.data.response;
                this.coefficients.list = coefficients;
            });

            this.$http.get('/api/profile/sensitivities', options).then((response) => {
                let sensitivities = response.data.response;
                this.sensitivities.list = sensitivities;
            });

            this.$http.get('/api/profile/subscriptions', options).then((response) => {
                let subscriptions = response.data.response;
                this.subscriptions = subscriptions;
            });

        },

        addField(section) {
            let object = Object.assign({}, section.template);
            section.list.push(object);
        },

        removeField(section, element) {
            let index = section.list.indexOf(element);
            section.list.splice(index, 1);
        },

        saveAllData() {
            let requestData = {
                user: this.user,
                coefficients: this.coefficients.list,
                sensitivities: this.sensitivities.list,
                pills: this.pills.list,
                sleeping: this.sleeping,
                subscriptions: this.subscriptions,
            };

            let options = {
                params: {
                    api_token: Laravel.apiToken
                }
            };

            this.$http.put('/api/profile/saveData', requestData, options).then((response) => {
                this.$swal('Збережено', 'Ваш профіль вдало оновився!', 'success');
            });
        },

        // subscribe(element) {
        //     let requestData = {
        //         subscriptions: this.subscriptions,

        //     };
        //     console.log(requestData);
        //     let options = {
        //         params: {
        //             api_token: Laravel.apiToken
        //         }
        //     };

        //     this.$http.put('/api/profile/subscribe', requestData, options).then((response) => {
        //         this.$swal('Збережено', 'Ваш профіль вдало оновився!', 'success');
        //     });
        // }


    },
});