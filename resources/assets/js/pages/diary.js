Vue.component('record', {
    template: '#record-template',
    props: ['record'],
    mounted() {},
    methods: {
        removeRecord() {
            let options = $.extend(true, {}, http_options);
            options.params.record = this.record;             

            this.$http.delete('/api/diary/deleteRecord', options).then((response) => {
                diary.reloadRecords();
            });  

        },

        saveRecord() {
            let requestData = {
                record: this.record,
                date: diary.date
            };            

            this.$http.post('/api/diary/addRecord', requestData, http_options).then((response) => {
                diary.reloadRecords();
                diary.setNewRecordsField();
            });
        },

        addProduct() {
            let product = Object.assign({}, diary.products.template);
            this.record.products.push(product);
        },
    },
});

const diary = new Vue({
    el: '#diary', 

    mounted() {
        this.setNewRecordsField();
        this.fetchData();
    },

    // watch: {
    //     date: function(value) {
    //         this.reloadRecords();
    //     }
    // },

    data: {
        success: false,

        date: moment().format('YYYY-MM-DD'),

        records: {
            template: {
                id: 0,
                time: null,
                sugar_level: null,
                bread_unity: null,
                insulin: null,
                pills: null,
                products: [],
            },
            list: [],
            new: {},
        },

        products: {
            template: {
                name: '',
                carbs: 0,
            },
            list: [],
        },

    },

    methods: {
        fetchData() {
            let options = {
                params: {
                    api_token: Laravel.apiToken
                }
            };

            this.$http.get('/api/diary/records', options).then((response) => {
                let records = response.data.response;
                this.records.list = records;
            });

        },

        setNewRecordsField() {
           this.records.new = Object.assign({}, this.records.template);
        },


        reloadRecords() {
            let options = $.extend(true, {}, http_options);
            options.params.date = this.date;     

            this.$http.get('/api/diary/records', options).then((response) => {
                let records = response.data.response;
                this.records.list = records;
            });
        }
    },

});