<?php

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', 'PagesController@home')->name('home');
Route::group(['middleware' => 'auth'], function () {

	Route::get('profile', 'ProfileController@profile')->name('profile');
	Route::get('records', 'DiaryController@records')->name('diary');
	Route::get('users', 'AllUsersController@users')->name('users');

});

Route::get('/products', 'ProductsController@products')->name('products');
Route::get('/getProducts', 'Api\ProductsController@getProducts');