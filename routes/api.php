<?php

use Illuminate\Http\Request;

Route::post('add/product', 'Api\ProductsController@add')->name('api.add.product');
Route::put('delete/product', 'Api\ProductsController@delete')->name('api.delete.product');

Route::group(['middleware' => 'auth:api', 'namespace' => 'Api'], function () {

	Route::group(['prefix' => 'profile'], function () {
		Route::get('', 'ProfileController@user');
		Route::get('coefficients', 'ProfileController@coefficients');
		Route::get('sensitivities', 'ProfileController@sensitivities');
		Route::get('pills', 'ProfileController@pills');
		Route::get('sleeping', 'ProfileController@sleeping');
		Route::put('saveData', 'ProfileController@saveData');
		Route::get('subscriptions', 'ProfileController@subscriptions');
		Route::put('subscribe', 'ProfileController@subscribe');

	});

	Route::group(['prefix' => 'diary'], function () {
		Route::get('records', 'DiaryController@records');
		Route::delete('deleteRecord', 'DiaryController@deleteRecord');
		Route::post('addRecord', 'DiaryController@addRecord');
	});

});