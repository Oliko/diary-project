/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "./";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 161);
/******/ })
/************************************************************************/
/******/ ({

/***/ 131:
/***/ (function(module, exports) {

var profile = new Vue({
    el: '#profile',

    mounted: function mounted() {
        this.fetchData();
    },


    data: {
        success: false,

        user: {},

        coefficients: {
            template: {
                time_from: null,
                time_to: null,
                value: null
            },
            list: []
        },

        sensitivities: {
            template: {
                time_from: null,
                time_to: null,
                value: null
            },
            list: []
        },

        pills: {
            template: {
                name: null,
                dose: null,
                notes: null,
                time: null,
                allow_notifications: true
            },
            list: []
        },

        sleeping: {
            time_from: null,
            time_to: null,
            allow_notifications: true
        },

        subscriptions: []

    },
    methods: {
        fetchData: function fetchData() {
            var _this = this;

            var options = {
                params: {
                    api_token: Laravel.apiToken
                }
            };

            this.$http.get('/api/profile', options).then(function (response) {
                var user = response.data.response;
                _this.user = user;
            });

            this.$http.get('/api/profile/sleeping', options).then(function (response) {
                var sleeping = response.data.response;
                _this.sleeping = sleeping;
            });

            this.$http.get('/api/profile/pills', options).then(function (response) {
                var pills = response.data.response;
                _this.pills.list = pills;
            });

            this.$http.get('/api/profile/coefficients', options).then(function (response) {
                var coefficients = response.data.response;
                _this.coefficients.list = coefficients;
            });

            this.$http.get('/api/profile/sensitivities', options).then(function (response) {
                var sensitivities = response.data.response;
                _this.sensitivities.list = sensitivities;
            });

            this.$http.get('/api/profile/subscriptions', options).then(function (response) {
                var subscriptions = response.data.response;
                _this.subscriptions = subscriptions;
            });
        },
        addField: function addField(section) {
            var object = Object.assign({}, section.template);
            section.list.push(object);
        },
        removeField: function removeField(section, element) {
            var index = section.list.indexOf(element);
            section.list.splice(index, 1);
        },
        saveAllData: function saveAllData() {
            var _this2 = this;

            var requestData = {
                user: this.user,
                coefficients: this.coefficients.list,
                sensitivities: this.sensitivities.list,
                pills: this.pills.list,
                sleeping: this.sleeping,
                subscriptions: this.subscriptions
            };

            var options = {
                params: {
                    api_token: Laravel.apiToken
                }
            };

            this.$http.put('/api/profile/saveData', requestData, options).then(function (response) {
                _this2.$swal('Збережено', 'Ваш профіль вдало оновився!', 'success');
            });
        }
    }
});

/***/ }),

/***/ 161:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(131);


/***/ })

/******/ });