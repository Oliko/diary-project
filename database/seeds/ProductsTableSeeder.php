<?php

use App\Models\ProductCategory;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    public function run()
    {
    	Product::truncate();

    	$faker = Faker\Factory::create();

    	for($i = 1; $i <= 30; $i++) 
    	{
    		$product = new Product;
    		$product->name = $faker->word;
            $product->carbs = $faker->numberBetween($min = 10, $max = 100);
            $product->glycemic_index = $faker->numberBetween($min = 10, $max = 100);

    		ProductCategory::get()->random()->products()->save($product);
    	}
    }
}
