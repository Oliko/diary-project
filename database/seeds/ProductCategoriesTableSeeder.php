<?php

use App\Models\ProductCategory;
use Illuminate\Database\Seeder;

class ProductCategoriesTableSeeder extends Seeder
{
    public function run()
    {
    	ProductCategory::truncate();

    	$faker = Faker\Factory::create();

    	for($i = 1; $i <= 5; $i++) 
    	{
    		$category = new ProductCategory;
    		$category->name = $faker->word;
    		$category->save();
    	}
    }
}
