<?php

use App\Models\User;
use App\Models\SleepingTimetable;
use App\Models\Pill;
use App\Models\Coefficient;
use App\Models\SensitivityFactor;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
    	User::truncate();

        $user = new User;
        $user->name = "User";
        $user->email = "user@gmail.com";
        $user->admin = true;
        $user->password = bcrypt(1);
        $user->api_token = str_random(60);
        $user->save();

        $coefficients = new Coefficient([
            'time_from' => "10:00:00",
            'time_to' => "18:00:00",
            'value' => "2",
        ]);

        $sensitivities = new SensitivityFactor([
            'time_from' => "10:00:00",
            'time_to' => "18:00:00",
            'value' => "3",
        ]);

        $sleepingTimetable = new SleepingTimetable([
            'time_from' => "23:00:00",
            'time_to' => "09:00:00",
        ]);

        $pills = new Pill([
            'name' => "Ибупрофен",
            'dose' => "1 р/д",
            'notes' => "після їжі",
            'time' => "12:00:00",
        ]);

        $user->sleeping()->save($sleepingTimetable);
        $user->pills()->save($pills);
        $user->coefficients()->save($coefficients);
        $user->sensitivities()->save($sensitivities);
    }
}
